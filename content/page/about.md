<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [my history](#my-history)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

My name is The Dude. I have the following qualities:

- I rock a great beard
- I'm extremely loyal to my friends
- I like bowling

That rug really tied the room together.

### my history

To be honest, I'm having some trouble remembering right now, so why don't you
just watch [my movie](https://en.wikipedia.org/wiki/The_Big_Lebowski) and it
will answer **all** your questions.
