## Co-creating the outline of the Sentient Commons
####  what it will do, why this is valuable, its processes and its required nodes.
##### All as a (sketchy) pattern language, for clear but non-reductive structure, and to foster careful conscious development and refinement.

<figure><img src="/static/LogoDoodle.gif" alt="LogoSketch" title="Sketch of a Sentient commons Logo idea" width="400" height="400"  align="middle"/></figure>

---
Everything here is a sketch - including the logo..
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Introduction](#introduction)
- [Analysis (developmental sketch)](#analysis-developmental-sketch)
- [Proposition (developmental sketch)](#proposition-developmental-sketch)
  - [What are these models?](#what-are-these-models)
  - [Purpose (developmental sketch)](#purpose-developmental-sketch)
  - [Agreement (developmental sketch)](#agreement-developmental-sketch)
  - [Policies (developmental sketch)](#policies-developmental-sketch)
  - [Daily Life (developmental sketch)](#daily-life-developmental-sketch)
- [Call to Action](#call-to-action)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introduction
Talking to, and working with, many projects thinking deeply and working hard at the emerging shape of the next wave of civilisation - broadly, the implementation of new human institutions that have an explicit desire to self evolve in the direction of greater sanity, greater kindness, greater equity and greater liveability - it becomes clear that the breadth and scope of will, skill and developing wisdom is extraordinary, across an enormous and expanding range of domains and modes.

... and yet...

... there is a clear and frequently expressed frustration at the difficulty of deep collaboration and co-ordination, of effective support and learning modes, at the feeling that we are all too often working on similar approaches in varying degrees of isolation and often unstated / unacknowledged / uncomfortable competition.

"Why can't these projects work together more / be more aligned?" is often heard.

The Sentient Commons is a response to this situation: a clear proposition on the basis of a clear analysis. If these make sense to you, then your engagement with the development of this outline is requested.

## Analysis (developmental sketch)
The messages that must be exchanged between humans and human institutions in forming and implementing collaborations come in several varieties, each of which has a different 'carrier wave'. Broadly, these message types, their associated carrier waves and the intent of each can be identified as:

- **MANDATE** - messages with social weight : evidence of aggregate will/agency.
- **CAPACITY** - demonstrations of skill, capacity, cultural capabilities : evidence of ability to deliver.
- **VALUE PROPOSITION** - Deliverables, labour, currency/tokens, information : acceptable promises of future value.
- **OFFER/ACCEPTANCE** - negotiation  - expectations vs commitments, rights vs obligations : shared agreements.
- **FALLBACK** - conditions around accountability : agreed dispute resolution processes.

These characterisations, though, are highly abstract: message type, carrier wave, broad intent. The signals communicated can be encoded in various ways - just as the encoding of radio signals can carry radio, morse code or television. The encodings here are cultural, and as Marshall McLuhan famously observed, they are not neutral. In fact, once they are established, their systemic character and the cultural modes they support and (just as crucially) inhibit are ultimately of more significance than the individual messages they encode.

If we ask what are the predominant cultural 'media' through which these messages are encoded, what are the archetypes that underly these media, we find these:

- **MANDATE** - Corporate legal personhood, Nation State, Treaty Organisation.
- **CAPACITY** - Monopolies of one kind or another: property rights, means of production, official qualifications, proof of contribution to previous work, employees, intellectual property.
- **VALUE PROPOSITION** - IP rights, property rights. usage rights, waged labour, commodity-framed money.
- **OFFER/ACCEPTANCE** - Contract Law as defined by Nation States and International Treaty.
- **FALLBACK** - Nation State as enforcer - with implicit willingness to resort to violence.

Looking at these, it becomes clear that one reason for the difficulties in collaboration described above arise is that **all of these culturally dominant modes of messaging are direct inhibitors of the social relations we want to build, support and develop.**

To be more direct, if one new economy organisation wishes to collaborate with another, the messages around the viability of each partner, how they demonstrate their capacity to deliver, their conversation around the distribution of the future value of the work, how they formalise their agreement, and what will happen should either of them fail to deliver - all of these must now take place in language and in terms which we are explicit critics of - through media which we know condition what can be communicated in ways that are at best limiting, likely to be distorting, and are quite possibly destructive.

Of course, there are many projects proposing to address one or other of these media - promoting better models, alternate approaches, modulations of the existing - all sorts of great ideas are in evidence.

But crucially, there exists no interaction space within which alternate versions of all five modalities are present, coherent, mutually supportive and reasonably stable.

Collaborations generally seem to attempt one of two broad approaches to address this situation - *either* to accept the situation is it is and attempt to become good navigators of the systems in place, looking for ways through them that are least damaging, *or* to attempt to downplay the need for some or all of these modalities - try and do without them (not make them explicit or indeed simply fail to acknowledge them).

There are clear reasons why both of these approaches are sub-optimal - the first tends to result in co-option / assimilation at one level or another (an org ceases to be radical), while the second usually results in project breakdown of some kind, or failure to deliver meaningful change.

**To sum up, it seems clear that problems around collaboration and co-ordination which have significant inhibitory effect upon the birthing of the new civilisational modes that are necessary for avoidance of collapse are due in large part to enforced reliance upon the modes of communication used to negotiate these collaborations - modes which structurally and systemically embed damaging or limiting interactions.**

## Proposition (developmental sketch)

The collaboration negotiation messaging mediums currently prevalent work in two ways - explicit and implicit. Explicitly, they depend upon and are deeply embedded into the social infrastructure of legal systems - legislators, courts, judges, lawyers, fines, prison (and ultimately dependent upon the will-to-violence of Nation States). Implicitly, they work through learned and reified cultural conditioning - we impose them upon ourselves because we know of no other way to feel confident in proceeding.

**The Sentient Commons** is a proposition to implement, as a global commons, a collaboration context where new and more constructive messaging is supported by explicit and transparent encodings. A place where projects, organisations, institutions and communities of all kinds can interact and collaborate on the basis of supportive frameworks of communication around the four fundamentals.

This is not a secessionist idea - rather, it is about building a new cultural space for institutions that can exist in parallel with the current hegemonic order, but in a different dimension; orthogonal, as it were.

No organisation need change anything about itself in order to take part. No organisation need cease participating in the contemporary economy to take part. No organisation need commit to any collaboration in order to take part.

Organisations should only join if they see a value in being in communion with others on the basis of the new models of negotiation, collaboration, exchange and accountability that are developed and supported in this space.

### What are these models?

It is important to be clear that this initiative is not intended to develop singular replacements for the sub-optimal methods in current use - to replace one hegemony with another. Rather, the intent is to become a safe context within which parties may choose their mode of engagement from a number of propositions - and equally to engage with the development and refinement of those propositions.

The Sentient Commons will seek to be a place which discourages hegemony, and encourages conscious evolution.

It is clear from the work of many groups that alternate models do exist, in a wide variety of states of development, readiness and establishment. The approach would be to invite the developers, proponents and users of these models into the Sentient Commons, on the basis of a proposition that valuable synergies and support mechanisms can be developed when they  operate in conjunction.

A 'first pass' survey would suggest the following principal classes of alternative:

- **MANDATE** - Mutual Agreement constitutions, novel democratic processes, DAOs.
- **CAPACITY** - Commonly held assets, Multi-metric/ federated reputation, resilient org structures.
- **VALUE PROPOSITION** - Common assets, Mutual Credit, Place- and Asset-based use credits and tokens.
- **OFFER/ACCEPTANCE** - Mutual Agreement, smart contracts, social promises.
- **FALLBACK** - Commons governance, commons arbitration based in precedent and principle, with enforcement limited to reputation and exclusion.

More specifically still, it is proposed that the Sentient Commons is established as a global, open membership, voluntary organisation to develop, support and instantiate carriers and interweavings for novel collaboration modalities.

The Sentient Commons should be broad, deep, and eventually quite technocratic in supporting its Members to find and use the modalities and media that most beneficially enable their collaborations with other Members to achieve synergies.

The following is a simplistic outline of the Sentient Commons from four perspectives: **Purpose**, **Agreement**, **Policy** and **Daily Life**, through which Members will interact to pursue their aims. If these are devised, instantiated, operated, maintained, evolved and stewarded, then there is a chance that the emergent culture will serve our intentions.

*Please remember the 'sketch' status of all of this! The intention is to spark engagement with the development of this proposition, so that it can become more alive.*

### Purpose (developmental sketch)
*Purpose is, once decided, essentially immutable. For as long as the Purpose motivates people to engage with the organisation, it has life. If it ceases to do this, then the organisation dies. If desirable, it can be cloned, at which point changes can be made freely - but the successor organisation will be a different creature, with its own life cycle.*

**The Purpose of The Sentient Commons is to establish, maintain and develop a socio-technical environment which develops and implements and supports modes of beneficial interaction between human institutions which work in ultimate service of the increased carrying capacity of the biosphere.**

### Agreement (developmental sketch)
*Agreement is the basis of Membership. It is subject to change, but with a relatively high democratic hurdle. Change at this level should be slow, careful and carefully debated. In the case of division between members as to significant changes at the Agreement level (all changes at this level are likely to be significant), the institution should consider a fork, rather than attempting to enforce some democratic decision upon a minority.
This may look like a boring old Members' Agreement, but if you read it, you'll see that it is quite radical, in its own way - being written deliberately to emphasise the requirement upon Members to work together in good faith for the furtherance of the Purpose, in the explicit absence of any mode of external enforcement.*


Members agree to:
1. Act with other Members in good faith to further the Purpose of the Sentient Commons.
1. Join with other Members to form a global, unincorporated voluntary association ('The Association') without reference to the jurisdiction of any Nation State or Treaty Organisation.
1. Abide by the Conditions of Membership, as follows:
   1. Each Member organisation desires to join The Association as an organisation (individual Memberships do not exist),
   1. The existing Membership consents (in accordance with Policies as agreed by Members) to the Membership of each Member.
   1. Membership will persist while both of these conditions are maintained, but not otherwise.
1. Act with other Members in good faith to democratically govern The Association in accordance with Policies as agreed by Members under the terms of this Agreement.
1. To support and enable the independence and freedom from coercion of a Sentient Commons Stewardship Panel in the maintenance, development and fulfilment of their role in delivering reasoned adjudications in all cases pertaining to the alteration and meaning of this Agreement, and additionally those deemed relevant in accordance with Policies as agreed by Members. The Stewardship Panel is to be formed of no less than 13 individuals selected by the Membership in accordance with Policies as agreed by Members.
1. To act with other Members to establish such groupings of Members as are deemed necessary to develop the Association's purpose.
1. To act with other Members in the negotiation of any agreements required with other bodies as are deemed necessary to develop the Association's purpose. Where practical, such agreements should be negotiated and formalised in accordance with processes and structures developed by The Association.
1. Ensure, along with other Members, that The Association, under the jurisdiction of any Nation State or Treaty Organisation:
   1. Neither acquires nor holds assets, securities, or other property.
   1. Neither assumes nor takes on any debt.
1. Arrange, with other Members, for any requirements in terms of assets, securities, property or debt to be managed on behalf of The Association by a suitable Stewardship Body, and managed as Common Assets/Liabilities.
1. Not have resort to the legislature of any Nation State or Treaty Organisation in the resolution of any dispute with another Member or with the Association as a whole.

### Policies (developmental sketch)
*Policies encompass those aspects of the culture of the organisation that need to be formalised. Policy should be developed on a minimum viable basis. Changing policies should be a straightforward matter, democratically controlled, but delegated where practical (for instance, if the policy is required for some technocratic, outcome requirement). Policy Jubilees are a good idea - something like a seven-yearly process during which all policies are repealed, and new policies introduced, one by one, as they are needed. Of course, these might simply be the old policies re-instated, but the process seems healthy.*

It is not proposed here to outline any specific Policies, but it is worth exploring the implications of several clauses from the sketch of the Membership Agreement set out above.

Clause 5 calls for the establishment and maintenance of a Commons Stewardship Panel. This is essentially a Supreme Court for the Sentient Commons (and potentially a nominated body for dispute resolution in regard of all kinds of agreements reached through the models developed and offered there). This panel consists of individuals elected by the Members. It is empowered to decide for itself how it manages dispute resolution, but is enjoined by the Agreement to deliver reasoned adjudications - that is, it must provide written argument as to its adjudications. Note, though, that there is no requirement upon Members to abide by these adjudications. They are not enforceable judgements, but opinions. This condition underpins the whole thrust of the Agreement, which is to engender a culture outside that of enforcement by external powers - to embody in The Association those new social relations which it intends to foster and develop.

Clause 6 empowers Members to set up any groupings they deem necessary. It is assumed that, as well as internal sub-groups charged with management, implementation, research and the like, these will include bodies that are constituted under some existing jurisdiction - for the purposes of holding assets, running bank accounts, entering into contracts. Note, though, that Clauses 8 and 9 will require these bodies to be Stewardship bodies - not agents, but empowered (probably under oversight of the Stewardship Panel) to veto instructions which do not align with their established remit.


### Daily Life (developmental sketch)
*Daily Life is the most important aspect of the culture of the Sentient Commons. Members should, by and large, go about their business in the context of The association as they see fit, without recourse to any rules. The Agreement and the Policies should seek to enable this to the maximum practical extent. Only in the case that a Member is concerned that some action may deemed in breach of Policy or Agreement terms should they consider them, and take advice.*

"Do anything you wanna do." *Eddie and the Hot Rods*
"Go easy .. Step lightly .. Stay free."  *The Clash*

*Once again, please do remember the 'sketch' status of all of this!*

## Call to Action

If you don't think is crazy, please engage - like this: [Use 'Issues' here](https://gitlab.com/the-sentient-commons/sentient-commons-outline/issues), or email Dil Green at protonmail dot com (name all one word).
thanks for reading!
    ---

<!-- [project Pages][projpages].

---

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
-->
